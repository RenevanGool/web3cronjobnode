const path = require("path");
var HDWalletProvider = require("truffle-hdwallet-provider");
const MNEMONIC = "join slim pizza surround pupil purse tank mountain auction believe fresh excite";

module.exports = {
  // See <http://truffleframework.com/docs/advanced/configuration>
  // to customize your Truffle configuration!
  contracts_build_directory: path.join(__dirname, "client/src/contracts"),
  networks: {
    ropsten: {
      provider: function() {
        return new HDWalletProvider(
          MNEMONIC, 
          "https://ropsten.infura.io/v3/e682beb2ba8848108b4217a0f44d780a",
          0,
          5
        );
      },
      network_id: "3",
      from: "0x2A66E34Ea78A9E51365F085d75b31394C547E1Ed",
    },
    develop: {
      port: 7545
    }
  },
  compilers: {
    solc: {
      version: "0.4.24"
    }
  }
};
