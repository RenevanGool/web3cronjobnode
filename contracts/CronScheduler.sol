pragma solidity 0.4.24;

import './Interface/SchedulerInterface.sol';

contract CronScheduler{
    SchedulerInterface public scheduler;
    
    uint lockedUntil;
    address public currentScheduledTransaction;

    event TaskScheduled(address indexed scheduledTransaction);
    event TaskExecuted(address indexed scheduledTransaction);
    uint sum;

    function CronScheduler(address _scheduler) public payable {
        scheduler = SchedulerInterface(_scheduler);
        lockedUntil = block.number + 5760;
        
        schedule();
    }

    function ()
        public payable 
    {
        if (msg.value > 0) { //this handles recieving remaining funds sent while scheduling (0.1 ether)
            return;
        } 
        
        process();
    }

    function process() public returns (bool) {
        calculate();
        schedule();
    }

    function calculate()
        public returns (bool)
    {
        require(block.number >= lockedUntil);
        sum = 6 + 8;
        emit TaskExecuted(currentScheduledTransaction);
        return true;
    }

    function schedule() 
        private returns (bool)
    {
        currentScheduledTransaction = scheduler.schedule.value(0.1 ether)( // 0.1 ether is to pay for gas, bounty and fee
            this,                   // send to self
            "",                     // and trigger fallback function
            [
                1000000,            // The amount of gas to be sent with the transaction. Accounts for payout + new contract deployment
                0,                  // The amount of wei to be sent.
                255,                // The size of the execution window.
                lockedUntil,        // The start of the execution window.
                20000000000 wei,    // The gasprice for the transaction (aka 20 gwei)
                20000000000 wei,    // The fee included in the transaction.
                20000000000 wei,         // The bounty that awards the executor of the transaction.
                30000000000 wei     // The required amount of wei the claimer must send as deposit.
            ]
        );

        emit TaskScheduled(currentScheduledTransaction);
    }

}